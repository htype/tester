package  ru.tester
import Struct._
import spray.routing.{RequestContext, HttpService}

import scala.concurrent.Future

import akka.actor.ActorSystem

import spray.httpx.SprayJsonSupport._
import spray.json.DefaultJsonProtocol._

import spray.httpx.encoding.{Gzip, Deflate}
import spray.httpx.SprayJsonSupport._
import spray.client.pipelining._
import spray.json._

import scala.util._

/**
 * Created by acer on 07.05.15.
 */
import spray.http._
import HttpHeaders._

class Tester private (var log: String, var pass: String) {
  import Struct.MyJsonProtocol._
  implicit val system: ActorSystem = ActorSystem()
  import system.dispatcher

  private val pipeline: HttpRequest => Future[String] = (
    sendReceive
      ~> decode(Gzip)
      ~> unmarshal[String]
    )

  private var isLogged = false
  private var user:User = null

  private def sendLoginRequest(func: => Unit): Unit = {
    val loginUrl = Tester.loginUrl + log
    val response = pipeline(Post(loginUrl, LogInfo(pass, false)))
    response onComplete {
      case Success(loggedUser) => {
        user = loggedUser.parseJson.convertTo[User]
        isLogged = true
        func
      }
      case Failure(msg) => {
        println(msg) //
      }
    }
  }

  private def safeRun(func: => Unit): Unit = { //runs function func only after login
    if (isLogged) func
    else {
      sendLoginRequest(func)
    }
  }

  def logIn(): Unit = {
    sendLoginRequest(println("Login(success):"+user) )
  }

  def showUserInfo() =  {
      safeRun {
        val userInfoUrl = Tester.userInfoUrl + user.userId
        val response = pipeline(Get(userInfoUrl) ~>
          getUserCookies(user))

        response onComplete {
          case Success(userInfo) => println("User info:\n"+userInfo)
          case Failure(info) => println(info)
        }
      }
  }

  def setUserName(newName: String): Unit = {
    safeRun {
      val userInfoUrl = Tester.userInfoUrl + user.userId
      val jsRequest = JsObject("about" -> "".toJson, "name" -> newName.toJson)
      val response = pipeline(Patch(userInfoUrl, jsRequest) ~>
        getUserCookies(user))

      response onComplete {
        case info => println(info)
      }
    }
  }

  def createMoment(title: String, content: String, storyId: String): Unit = {
    safeRun {
      val userInfoUrl = Tester.userMomentsUrl + storyId + "/moments?"
      val jsContent = s"""{"type": "article", "contents": [{"type": "p", "contents":[{"type": "text", "contents": "${content}"}] }] }"""
      val jsStory = JsObject("attachments" -> Map[Int,Int]().toJson,
                             "storyId"     -> storyId.toJson,
                             "title"       -> title.toJson,
                             "content"     -> jsContent.toJson
      )
      val response = pipeline(Post(userInfoUrl, jsStory) ~>
        getUserCookies(user))

      response onComplete {
        case Success(info) => println(info)
        case Failure(info) => println(info)
      }
    }
  }

  def deleteMoment(storyId: String, momentId: String): Unit = {
    safeRun {
      val userInfoUrl = Tester.userMomentsUrl + storyId + "/moments/" + momentId

      val response = pipeline(Delete(userInfoUrl) ~>
        getUserCookies(user))

      response onComplete {
        case Success(info) => println("Delete(success):" + info)
        case Failure(info) => println("Delete(fail):" + info.getCause)
      }
    }
  }
  //https://preprod.selfishbeta.com/api//core/stories/0844d91c9c46f000/moments/0846f00fc306f000?

  private def getUserCookies(user:User) =
      Cookie(HttpCookie("SSID", user.sessionId),
             HttpCookie("userId", user.userId),
             HttpCookie("accountId", user.accountId))
}

object Tester {

  def apply(login: String, password: String):Tester = {
    new Tester(login, password)
  }

  val loginUrl       = "https://preprod.selfishbeta.com/api//acl/auth/Selfish/"
  val userInfoUrl    = "https://preprod.selfishbeta.com/api//core/users/"
  val userMomentsUrl = "https://preprod.selfishbeta.com/api//core/stories/"
}

object Examples {
  val momentText = "There's a killer on the road, his brain is squirmin' like a toad.Take a long holiday, let your children play. If ya give this man a ride, sweet family will die. Killer on the road, yeah.stripMargin"
  val momentTitle = "Riders On The Storm"
}
