package ru.tester.Struct


import spray.httpx.SprayJsonSupport._
import spray.http._
import spray.json.DefaultJsonProtocol
import spray.httpx.encoding._
/**
 * Created by acer on 06.05.15.
 */
case class User(userId:String, accountId:String, sessionId:String)
case class LogInfo(password:String, remember: Boolean)

object MyJsonProtocol extends DefaultJsonProtocol {
  implicit val userFormat = jsonFormat3(User)
  implicit val logInfoFormat = jsonFormat2(LogInfo)
}
