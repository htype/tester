name := "Tester"

version := "1.0"

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  "io.spray" %%  "spray-can"     % "1.3.2",
  "io.spray" %%  "spray-routing" % "1.3.2",
  "io.spray" %%  "spray-httpx"   % "1.3.2",
  "io.spray" %%  "spray-http"    % "1.3.2",
  "io.spray" %%  "spray-json"    % "1.3.1",
  "io.spray" %%  "spray-client"  % "1.3.2",
  "io.spray" %%  "spray-util"    % "1.3.2",
  "com.typesafe.akka" %% "akka-actor" % "2.3.6"
)